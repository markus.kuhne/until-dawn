using UnityEngine;

public class Buoyancy : MonoBehaviour
{
    public float waterLevel = 0.0f; // Set this to the Y position of your water surface.
    public float floatHeight = 1.0f; // Adjust this value for desired buoyancy force.
    public float buoyancy = 2.0f; // Adjust this value for desired buoyancy force.
    public Vector3 buoyancyCenterOffset = Vector3.zero; // Adjust this offset for the object's center of buoyancy.

    private void FixedUpdate()
    {
        // Calculate the object's position relative to the water level.
        float posY = transform.position.y - waterLevel;

        // Check if the object is below the water surface.
        if (posY < 0)
        {
            // Calculate the buoyancy force based on the object's depth.
            float buoyancyForce = -posY * buoyancy;

            // Apply the buoyancy force at the center of buoyancy.
            Vector3 buoyancyForcePoint = transform.position + buoyancyCenterOffset;
            GetComponent<Rigidbody>().AddForceAtPosition(Vector3.up * buoyancyForce, buoyancyForcePoint);
        }
    }
}

