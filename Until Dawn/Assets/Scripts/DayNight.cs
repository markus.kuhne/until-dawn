using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNight : MonoBehaviour
{
    
    public Light directionalLight;
    public float cycleDuration = 60f;

    private float rotationSpeed;
    private float currentRotation;

    void Start()
    {
        rotationSpeed = 360f / cycleDuration;
    }

    // Update is called once per frame
    void Update()
    {
        currentRotation += rotationSpeed * Time.deltaTime;

        if (currentRotation >= 360f)
        {
            currentRotation -= 360f;
        }  

        directionalLight.transform.rotation = Quaternion.Euler (currentRotation, 0f, 0f);
    }
}
