using UnityEngine;

public class PauseResumeGame : MonoBehaviour
{
    private bool gamePaused = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (gamePaused)
                ResumeGame();
            else
                PauseGame();
        }
    }

    private void PauseGame()
    {
        if (!gamePaused)
        {
            Time.timeScale = 0f; // Pause the game by setting the time scale to 0
            gamePaused = true;
            Debug.Log("Game paused");
        }
    }

    private void ResumeGame()
    {
        if (gamePaused)
        {
            Time.timeScale = 1f; // Resume the game by setting the time scale back to 1
            gamePaused = false;
            Debug.Log("Game resumed");
        }
    }
}


